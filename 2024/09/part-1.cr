#!/usr/bin/env crystal

record(Span, value : Int32, size : Int32) do
  def initialize(@size)
    @value = -1
  end

  def self.empty(size : Int32)
    new(size)
  end

  def empty?
    @value < 0
  end
end

class SpanArray
  include Indexable::Mutable(Span)

  def initialize(@spans = [] of Span)
  end

  def self.parse(io : IO) : self
    spans = [] of Span
    value = 0
    files = true
    io.each_char do |char|
      next unless size = char.to_i?
      spans << (files ? Span.new(value, size) : Span.empty(size))
      value += 1 if files
      files = !files
    end
    new(spans)
  end

  def size
    @spans.size
  end

  def unsafe_fetch(index : Int)
    @spans.unsafe_fetch(index)
  end

  def unsafe_put(index : Int, value : Span)
    @spans.unsafe_put(index, value)
  end

  def add(span) : Nil
    @spans << span
  end

  def <<(span) : self
    @spans << span
    self
  end

  def trim
    @spans.pop
    while @spans.last.empty?
      @spans.pop
    end
  end

  def replace_at(index : Int, *spans) : Nil
    @spans.delete_at(index)
    @spans.insert_all(index, spans)
  end

  def each(&)
    @spans.each { |span| yield span }
  end

  def each_block(&)
    each do |span|
      span.size.times { yield span.value }
    end
  end

  def checksum
    sum = 0_i64
    index = -1
    each_block do |value|
      index += 1
      next if value < 0
      sum += value * index
    end
    sum
  end

  def to_s(io : IO) : Nil
    each_block do |value|
      if value >= 0
        value.to_s(io)
      else
        io << '.'
      end
    end
  end
end

def compact(array)
  dest_index = 0
  while dest_index < array.size
    src_span = array[-1]
    dest_span = array[dest_index]

    if src_span.empty?
      array.trim
      next
    elsif !dest_span.empty?
      dest_index += 1
      next
    end

    if src_span.size > dest_span.size
      array[dest_index] = src_span.copy_with(size: dest_span.size)
      array[-1] = src_span.copy_with(size: src_span.size - dest_span.size)
      dest_index += 1
    elsif src_span.size == dest_span.size
      array[dest_index] = src_span
      array.trim
      dest_index += 1
    else
      array.replace_at(dest_index, src_span, dest_span.copy_with(size: dest_span.size - src_span.size))
      array.trim
      dest_index += 1
    end

    STDERR.puts array if DEBUG
  end
end

DEBUG = false

array = SpanArray.parse(STDIN)
STDERR.puts array if DEBUG
compact(array)
puts array.checksum
