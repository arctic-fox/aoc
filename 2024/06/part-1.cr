#!/usr/bin/env crystal

require "bit_array"

enum Direction
  North = 0
  East
  South
  West

  def self.from_char(char)
    case char
    when '^' then North
    when '>' then East
    when 'V' then South
    when '<' then West
    else          raise "Bad direction char"
    end
  end

  def turn
    self.class.from_value((value + 1) % 4)
  end

  def to_char
    case self
    in .north? then '^'
    in .east?  then '>'
    in .south? then 'V'
    in .west?  then '<'
    end
  end
end

class Simulation
  @visited = Set({Int32, Int32}).new

  def initialize(@cells : Array(BitArray), @position : {Int32, Int32}, @direction : Direction)
  end

  def self.parse(io)
    position = {0, 0}
    direction = Direction::North
    cells = io.each_line.map_with_index do |line, y|
      BitArray.new(line.size) do |x|
        case line[x]
        when '.' then false
        when '#' then true
        else
          direction = Direction.from_char(line[x])
          position = {x, y}
          false
        end
      end
    end
    new(cells, position, direction)
  end

  def self.run(io)
    parse(io).run
  end

  def run
    while step
      # ...
    end
    visited
  end

  def step
    return if done?
    if obstacle?
      turn
    else
      move_forward
    end
  end

  private def obstacle?
    !!front
  end

  private def turn
    @direction = @direction.turn
  end

  private def move_forward
    @visited << @position
    @position = front_position
  end

  private def front_position
    case @direction
    in .north? then {@position[0], @position[1] - 1}
    in .east?  then {@position[0] + 1, @position[1]}
    in .south? then {@position[0], @position[1] + 1}
    in .west?  then {@position[0] - 1, @position[1]}
    end
  end

  private def front
    x, y = front_position
    @cells[y]?.try &.[x]?
  end

  def visited
    @visited.size
  end

  def done?
    @position[0] < 0 ||
      @position[1] < 0 ||
      @position[0] >= width ||
      @position[1] >= height
  end

  def width
    @cells[0].size
  end

  def height
    @cells.size
  end

  def to_s(io : IO) : Nil
    @cells.each_with_index do |row, y|
      row.each_with_index do |cell, x|
        char = case
               when @position == {x, y}  then @direction.to_char
               when {x, y}.in?(@visited) then 'X'
               else                           cell ? '#' : '.'
               end
      end
    end
  end
end

puts Simulation.run(STDIN)
