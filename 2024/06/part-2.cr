#!/usr/bin/env -S crystal run --release

require "bit_array"

enum Direction
  North = 0
  East
  South
  West

  def self.from_char(char)
    case char
    when '^' then North
    when '>' then East
    when 'V' then South
    when '<' then West
    else          raise "Bad direction char"
    end
  end

  def turn
    self.class.from_value((value + 1) % 4)
  end

  def offset
    case self
    in .north? then {0, -1}
    in .east?  then {1, 0}
    in .south? then {0, 1}
    in .west?  then {-1, 0}
    end
  end

  def apply(x, y)
    off_x, off_y = offset
    {x + off_x, y + off_y}
  end

  def to_char
    case self
    in .north? then '^'
    in .east?  then '>'
    in .south? then 'V'
    in .west?  then '<'
    end
  end
end

class Simulation
  getter start_position : {Int32, Int32}
  getter start_direction : Direction

  getter position : {Int32, Int32}
  getter direction : Direction

  @visited = Set({Int32, Int32, Direction}).new

  def initialize(@cells : Array(BitArray), @position : {Int32, Int32}, @direction : Direction)
    @start_position = position
    @start_direction = direction
  end

  def clone
    cells = Array.new(height) do |y|
      BitArray.new(width) { |x| @cells[y][x] }
    end
    Simulation.new(cells, @start_position, @start_direction)
  end

  def self.parse(io)
    position = {0, 0}
    direction = Direction::North
    cells = io.each_line.map_with_index do |line, y|
      BitArray.new(line.size) do |x|
        case line[x]
        when '.' then false
        when '#' then true
        else
          direction = Direction.from_char(line[x])
          position = {x, y}
          false
        end
      end
    end
    new(cells, position, direction)
  end

  def self.run(io)
    parse(io).run
  end

  def run
    while step
      # ...
    end
    visited
  end

  def step
    return if done?
    visit_position
    if obstacle?
      turn
    else
      move_forward
    end
  end

  private def obstacle?
    !!front
  end

  private def turn
    @direction = @direction.turn
  end

  private def move_forward
    @position = front_position
  end

  private def visit_position
    @visited << {*@position, @direction}
  end

  private def front_position
    @direction.apply(*@position)
  end

  private def front
    x, y = front_position
    return if x < 0 || x >= width || y < 0 || y >= height
    @cells[y][x]
  end

  def block(x, y)
    return if x < 0 || y < 0 || x >= width || y >= height
    return false if @cells[y][x]
    @cells[y][x] = true
    true
  end

  def visited
    @visited.size
  end

  def done?
    x < 0 || y < 0 || x >= width || y >= height || loop?
  end

  def loop?
    @visited.includes?({*@position, @direction})
  end

  def x
    @position[0]
  end

  def y
    @position[1]
  end

  def width
    @cells[0].size
  end

  def height
    @cells.size
  end

  def to_s(io : IO) : Nil
    @cells.each_with_index do |row, y|
      row.each_with_index do |cell, x|
        north = {x, y, Direction::North}.in?(@visited)
        east = {x, y, Direction::East}.in?(@visited)
        south = {x, y, Direction::South}.in?(@visited)
        west = {x, y, Direction::West}.in?(@visited)
        cross = (north || south) && (east || west)
        char = case
               when @position == {x, y} then @direction.to_char
               when cross               then '+'
               when north, south        then '|'
               when west, east          then '-'
               when cell                then '#'
               else                          '.'
               end
        io << char
      end
      io.puts
    end
  end

  def [](x, y)
    @cells[y][x]
  end

  def ==(other : self)
    height.times do |y|
      width.times do |x|
        return false if @cells[y][x] != other[x, y]
      end
    end
    true
  end
end

DEBUG = false

base = Simulation.parse(STDIN)
loops = [] of Simulation
base.height.times do |y|
  base.width.times do |x|
    next if base.start_position == {x, y}
    branch = base.clone
    next unless branch.block(x, y)
    branch.run
    next unless branch.loop?
    next if branch.position != base.start_position && branch.direction != base.start_direction
    next if loops.any? &.== branch
    loops << branch
    STDERR.puts branch if DEBUG
  end
end
puts loops.size
