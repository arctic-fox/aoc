#!/usr/bin/env crystal

alias Point = {Int32, Int32}

class Grid
  def initialize(@rows : Array(Array(Char)))
  end

  def self.parse(io)
    rows = [] of Array(Char)
    io.each_line do |line|
      rows << line.chars
    end
    new(rows)
  end

  def width
    @rows[0].size
  end

  def height
    @rows.size
  end

  def [](x, y)
    @rows[y][x]
  end

  def contains?(x, y)
    x.in?(0...width) && y.in?(0...height)
  end

  def each_antenna_pair(&) : Nil
    pairs_by_type = Hash(Char, Array(Point)).new do |hash, key|
      hash[key] = [] of Point
    end

    @rows.each_with_index do |row, y|
      row.each_with_index do |cell, x|
        next if cell == '.'
        pairs_by_type[cell] << {x, y}
      end
    end

    pairs_by_type.each_value do |antennas|
      antennas.each_combination(2, reuse: true) { |pair| yield pair[0], pair[1] }
    end
  end

  def print_with_antinodes(io, antinodes) : Nil
    height.times do |y|
      width.times do |x|
        next print '#' if {x, y}.in?(antinodes)
        print self[x, y]
      end
      puts
    end
  end
end

def reduce_points(a, b, &)
  x = yield a[0], b[0]
  y = yield a[1], b[1]
  {x, y}
end

DIFF = ->(a : Int32, b : Int32) { a - b }
ADD  = ->(a : Int32, b : Int32) { a + b }

def find_antinodes(a, b)
  diff_ab = reduce_points(a, b, &DIFF)
  diff_ba = reduce_points(b, a, &DIFF)
  antinode1 = reduce_points(a, diff_ab, &ADD)
  antinode2 = reduce_points(b, diff_ba, &ADD)
  {antinode1, antinode2}
end

DEBUG = false

grid = Grid.parse(STDIN)
antinodes = Set(Point).new
grid.each_antenna_pair do |a, b|
  find_antinodes(a, b).each do |node|
    antinodes << node if grid.contains?(*node)
  end
end
grid.print_with_antinodes(STDERR, antinodes) if DEBUG
puts antinodes.size
