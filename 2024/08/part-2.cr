#!/usr/bin/env crystal

alias Point = {Int32, Int32}

class Grid
  def initialize(@rows : Array(Array(Char)))
  end

  def self.parse(io)
    rows = [] of Array(Char)
    io.each_line do |line|
      rows << line.chars
    end
    new(rows)
  end

  def width
    @rows[0].size
  end

  def height
    @rows.size
  end

  def [](x, y)
    @rows[y][x]
  end

  def contains?(x, y)
    x.in?(0...width) && y.in?(0...height)
  end

  def each_antenna_pair(&) : Nil
    pairs_by_type = Hash(Char, Array(Point)).new do |hash, key|
      hash[key] = [] of Point
    end

    @rows.each_with_index do |row, y|
      row.each_with_index do |cell, x|
        next if cell == '.'
        pairs_by_type[cell] << {x, y}
      end
    end

    pairs_by_type.each_value do |antennas|
      antennas.each_combination(2, reuse: true) { |pair| yield pair[0], pair[1] }
    end
  end

  def print_with_antinodes(io, antinodes) : Nil
    height.times do |y|
      width.times do |x|
        next print '#' if {x, y}.in?(antinodes)
        print self[x, y]
      end
      puts
    end
  end
end

def reduce_points(a, b, &)
  x = yield a[0], b[0]
  y = yield a[1], b[1]
  {x, y}
end

DIFF = ->(a : Int32, b : Int32) { b - a }
ADD  = ->(a : Int32, b : Int32) { a + b }

def each_antinode_inner(a, b, width, height)
  prev = a
  node = b
  loop do
    yield node
    diff = reduce_points(prev, node, &DIFF)
    temp = node
    node = reduce_points(temp, diff, &ADD)
    prev = temp
    break unless node[0].in?(0...width) && node[1].in?(0...height)
  end
end

def each_antinode(a, b, width, height)
  each_antinode_inner(a, b, width, height) { |node| yield node }
  each_antinode_inner(b, a, width, height) { |node| yield node }
end

DEBUG = false

grid = Grid.parse(STDIN)
antinodes = Set(Point).new
grid.each_antenna_pair do |a, b|
  each_antinode(a, b, grid.width, grid.height) do |node|
    antinodes << node
  end
end
grid.print_with_antinodes(STDERR, antinodes) if DEBUG
puts antinodes.size
