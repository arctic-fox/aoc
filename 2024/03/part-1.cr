#!/usr/bin/env crystal

answer = 0
STDIN.gets_to_end.scan(/mul\((\d+),(\d+)\)/) do |match|
  a, b = match.captures.map &.not_nil!.to_i
  answer += a * b
end
puts answer
