#!/usr/bin/env crystal

answer = 0
enabled = true
STDIN.gets_to_end.scan(/(?:mul\((\d+),(\d+)\))|(?:do\(\))|(?:don't\(\))/) do |match|
  if match[0].starts_with?("don't")
    enabled = false
  elsif match[0].starts_with?("do")
    enabled = true
  elsif enabled
    a, b = match.captures.map &.not_nil!.to_i
    answer += a * b
  end
end
puts answer
