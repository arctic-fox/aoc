#!/usr/bin/env crystal

alias Position = {Int32, Int32}

struct Machine
  getter a : Position
  getter b : Position
  getter prize : Position

  def initialize(@a, @b, @prize)
  end

  def self.parse(io : IO) : self
    a = nil.as(Position?)
    b = nil.as(Position?)
    prize = nil.as(Position?)

    io.each_line do |line|
      break if line.empty?
      match = line.match(/(Button [AB]|Prize): X=?([-+]?\d+), Y=?([-+]?\d+)/)
      raise "Unexpected line: #{line}" unless match
      pos = {$2.to_i, $3.to_i}
      case $1
      when "Button A" then a = pos
      when "Button B" then b = pos
      else                 prize = pos
      end
    end

    raise "Missing A, B, or Prize" unless a && b && prize
    new(a, b, prize)
  end
end

struct Simulation
  A_COST =   3
  B_COST =   1
  MAX    = 100

  def initialize(@machine : Machine)
    @cost = 0
  end

  def solve
    cheapest = nil
    MAX.times do |a|
      MAX.times do |b|
        x = @machine.a[0] * a + @machine.b[0] * b
        y = @machine.a[1] * a + @machine.b[1] * b
        next if {x, y} != @machine.prize
        cost = a * A_COST + b * B_COST
        cheapest = cost if !cheapest || cost < cheapest
      end
    end
    cheapest
  end
end

machines = [] of Machine
until STDIN.peek.try &.empty?
  machines << Machine.parse(STDIN)
end
answer = machines.compact_map do |machine|
  Simulation.new(machine).solve
end.sum
puts answer
