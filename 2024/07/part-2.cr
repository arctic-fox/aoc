#!/usr/bin/env crystal

enum Operator
  Add
  Multiply
  Concatenate

  def apply(a, b)
    case self
    in .add?         then a + b
    in .multiply?    then a * b
    in .concatenate? then a.class.new("#{a}#{b}")
    end
  end
end

sum = 0_i64
STDIN.each_line do |line|
  result, nums = line.split(": ")
  result = result.to_i64
  operands = nums.split.map &.to_i64
  Operator.values.each_repeated_permutation(operands.size - 1, true) do |operators|
    i = 0
    try = operands.reduce do |a, b|
      operators[i].apply(a, b).tap { i += 1 }
    end
    next if try != result
    sum += result
    break
  end
end
puts sum
