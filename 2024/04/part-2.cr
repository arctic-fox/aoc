#!/usr/bin/env crystal

DEBUG = false
PATHS = {
  { {'M', -1, -1}, {'M', 1, -1}, {'S', -1, 1}, {'S', 1, 1} },
  { {'S', -1, -1}, {'M', 1, -1}, {'S', -1, 1}, {'M', 1, 1} },
  { {'S', -1, -1}, {'S', 1, -1}, {'M', -1, 1}, {'M', 1, 1} },
  { {'M', -1, -1}, {'S', 1, -1}, {'M', -1, 1}, {'S', 1, 1} },
}

def check_path(x, y, path, grid)
  path.each do |(char, x_off, y_off)|
    x2 = x + x_off
    y2 = y + y_off
    return if x2 < 0 || y2 < 0
    return unless row = grid[y2]?
    return unless row[x2]? == char
  end
  STDERR.puts "Found at (#{x}, #{y})" if DEBUG
  true
end

grid = [] of Array(Char)
STDIN.each_line do |line|
  grid << line.chars
end

answer = 0
grid.each_with_index do |row, y|
  row.each_with_index do |char, x|
    next unless char == 'A'

    PATHS.each do |path|
      answer += 1 if check_path(x, y, path, grid)
    end
  end
end
puts answer
