#!/usr/bin/env crystal

DEBUG = false
PATHS = {
  { {1, 0}, {2, 0}, {3, 0} },    # Horizontal
  { {0, 1}, {0, 2}, {0, 3} },    # Vertical
  { {1, 1}, {2, 2}, {3, 3} },    # Diagonal
  { {1, -1}, {2, -2}, {3, -3} }, # Diagonal
}

def check_path(x, y, path, grid)
  path.zip({'M', 'A', 'S'}) do |(x_off, y_off), char|
    x2 = x + x_off
    y2 = y + y_off
    return if x2 < 0 || y2 < 0
    return unless row = grid[y2]?
    return unless row[x2]? == char
  end
  STDERR.puts "Found at (#{x}, #{y}) to (#{x + path[2][0]}, #{y + path[2][1]})" if DEBUG
  true
end

def flip(path)
  path.map do |(x, y)|
    {x * -1, y * -1}
  end
end

grid = [] of Array(Char)
STDIN.each_line do |line|
  grid << line.chars
end

answer = 0
grid.each_with_index do |row, y|
  row.each_with_index do |char, x|
    next unless char == 'X'

    PATHS.each do |path|
      answer += 1 if check_path(x, y, path, grid)
      answer += 1 if check_path(x, y, flip(path), grid)
    end
  end
end
puts answer
