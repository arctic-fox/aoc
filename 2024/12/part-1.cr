#!/usr/bin/env crystal

require "bit_array"

@[Flags]
enum Direction
  North
  East
  South
  West
end

record Cell, x : Int32, y : Int32, open : Direction

class Region
  getter type : Char
  getter cells : Array(Cell)

  def initialize(@type, @cells)
  end

  def area : Int32
    @cells.size
  end

  getter perimeter : Int32 do
    value = area * 4
    @cells.each do |cell|
      cell.open.each { value -= 1 }
    end
    value
  end

  def price
    area * perimeter
  end
end

struct Mask
  getter width : Int32
  getter height : Int32

  def initialize(@width, @height)
    @bits = BitArray.new(width * height)
  end

  def [](x : Int, y : Int)
    index = coords_to_index(x, y)
    @bits[index]
  end

  def []=(x : Int, y : Int, value : Bool)
    index = coords_to_index(x, y)
    @bits[index] = value
  end

  private def coords_to_index(x, y)
    raise IndexError.new if x < 0 || y < 0 || x >= width || y >= height
    y * width + x
  end

  def filled?
    @bits.all?
  end

  def next_unmarked
    return unless index = @bits.index(false)
    index_to_coords(index)
  end

  private def index_to_coords(index)
    y, x = index.divmod(width)
    {x, y}
  end
end

class Grid
  def initialize(@rows : Array(Array(Char)))
  end

  def self.parse(io : IO) : self
    rows = [] of Array(Char)
    io.each_line do |line|
      rows << line.chars
    end
    new(rows)
  end

  def width
    @rows[0].size
  end

  def height
    @rows.size
  end

  def area
    width * height
  end

  def each_region(&)
    visited = Mask.new(width, height)
    while coords = visited.next_unmarked
      yield flood_fill(*coords, visited)
    end
  end

  private def flood_fill(x, y, visited)
    queue = [{x, y}]
    cells = [] of Cell
    visited[x, y] = true
    while coords = queue.pop?
      x, y = coords
      value = @rows[y][x]
      open = Direction::None
      each_neighbor(x, y) do |x2, y2, direction|
        next if @rows[y2][x2] != value
        open |= direction
        next if visited[x2, y2]
        queue << {x2, y2} unless visited[x2, y2]
        visited[x2, y2] = true
      end
      cells << Cell.new(x, y, open)
    end
    value = @rows[y][x]
    Region.new(value, cells)
  end

  private def each_neighbor(x, y, &)
    each_direction do |x_off, y_off, direction|
      x2 = x + x_off
      y2 = y + y_off
      next if x2 < 0 || y2 < 0 || x2 >= width || y2 >= height
      yield x2, y2, direction
    end
  end

  private def each_direction(&)
    yield 0, -1, Direction::North
    yield 1, 0, Direction::East
    yield 0, 1, Direction::South
    yield -1, 0, Direction::West
  end
end

grid = Grid.parse(STDIN)
answer = 0
grid.each_region do |region|
  answer += region.price
end
puts answer
