#!/usr/bin/env crystal

left = [] of Int32
right = [] of Int32

STDIN.each_line do |line|
  l, r = line.split.map &.to_i
  left << l
  right << r
end

tally = right.tally

answer = left.sum { |l| l * tally.fetch(l, 0) }
puts answer
