#!/usr/bin/env crystal

left = [] of Int32
right = [] of Int32

STDIN.each_line do |line|
  l, r = line.split.map &.to_i
  left << l
  right << r
end

left.sort!
right.sort!

answer = 0
left.zip(right) do |l, r|
  answer += (l - r).abs
end
puts answer
