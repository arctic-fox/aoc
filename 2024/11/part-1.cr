#!/usr/bin/env crystal

ITERATIONS = 25
DEBUG      = false

nums = STDIN.gets_to_end.split.map &.to_i64
STDERR.puts nums if DEBUG

ITERATIONS.times do |i|
  nums = nums.flat_map do |num|
    next 1 if num == 0
    num_str = num.to_s
    if num_str.size.even?
      half = num_str.size // 2
      left = num_str[...half].to_i64
      right = num_str[half..].to_i64
      [left, right]
    else
      num * 2024
    end
  end
  next unless DEBUG
  STDERR << i << ": " << nums
end
puts nums.size
