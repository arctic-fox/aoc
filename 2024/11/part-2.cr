#!/usr/bin/env crystal

ITERATIONS = 75
DEBUG      = true

nums = STDIN.gets_to_end.split.map &.to_i64
puts nums if DEBUG

ITERATIONS.times do |i|
  nums = nums.flat_map do |num|
    next 1 if num == 0
    num_str = num.to_s
    if num_str.size.even?
      half = num_str.size // 2
      left = num_str[...half].to_i64
      right = num_str[half..].to_i64
      [left, right]
    else
      num * 2024
    end
  end
  STDERR.puts i if DEBUG
end
puts nums.size
