#!/usr/bin/env crystal

class Grid
  def initialize(@rows : Array(Array(Int32)))
  end

  def self.parse(io : IO) : self
    rows = [] of Array(Int32)
    io.each_line do |line|
      row = line.chars.map &.to_i
      rows << row
    end
    new(rows)
  end

  def width
    @rows[0].size
  end

  def height
    @rows.size
  end

  def [](x : Int, y : Int)
    self[x, y]? || raise IndexError.new
  end

  def []?(x : Int, y : Int)
    return if x < 0 || x >= width || y < 0 || y >= height
    @rows[y][x]
  end

  private def each_direction(&)
    yield 0, -1
    yield 1, 0
    yield 0, 1
    yield -1, 0
  end

  def each_neighbor(x : Int, y : Int, &)
    each_direction do |x_off, y_off|
      x2 = x + x_off
      y2 = y + y_off
      next unless value = self[x2, y2]?
      yield x2, y2, value
    end
  end

  def each_traversable_neighbor(x : Int, y : Int, &)
    value = self[x, y]
    each_neighbor(x, y) do |x2, y2, value2|
      yield(x2, y2, value2) if value2 - value == 1
    end
  end

  def each_start(&)
    @rows.each_with_index do |row, y|
      row.each_with_index do |value, x|
        yield(x, y) if value == 0
      end
    end
  end

  def end?(x : Int, y : Int)
    self[x, y] == 9
  end

  def count_paths(x : Int, y : Int)
    queue = [{x, y}]
    count = 0
    while point = queue.pop?
      if end?(*point)
        count += 1
        next
      end
      each_traversable_neighbor(*point) do |x2, y2|
        queue << {x2, y2}
      end
    end
    count
  end
end

grid = Grid.parse(STDIN)
answer = 0
grid.each_start do |x, y|
  answer += grid.count_paths(x, y)
end
puts answer
