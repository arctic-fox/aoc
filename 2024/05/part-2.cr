#!/usr/bin/env crystal

def rule_satisfied?(pages, rule)
  a, b = rule
  return true unless index_a = pages.index(a)
  return true unless index_b = pages.index(b)
  index_a < index_b
end

def index_unsatisfied_rule(pages, rules)
  rules.index do |rule|
    !rule_satisfied?(pages, rule)
  end
end

def rules_satisfied?(pages, rules)
  !index_unsatisfied_rule(pages, rules)
end

def fix_rule(pages, rule)
  a, b = rule
  index_a = pages.index!(a)
  index_b = pages.index!(b)
  pages[index_b] = a
  pages[index_a] = b
end

def process_page_numbers(line, rules)
  pages = line.split(',').map &.to_i
  return if rules_satisfied?(pages, rules)
  while rule_index = index_unsatisfied_rule(pages, rules)
    fix_rule(pages, rules[rule_index])
  end
  pages[pages.size // 2]
end

def process_rule(line)
  a, b = line.split('|', 2).map &.to_i
  {a, b}
end

rules = [] of {Int32, Int32}
rules_completed = false

answer = 0
STDIN.each_line do |line|
  if line.blank?
    rules_completed = true
  elsif rules_completed
    if mid = process_page_numbers(line, rules)
      answer += mid
    end
  else
    rules << process_rule(line)
  end
end
puts answer
