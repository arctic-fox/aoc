#!/usr/bin/env crystal

def rule_satisfied?(pages, rule)
  a, b = rule
  return true unless index_a = pages.index(a)
  return true unless index_b = pages.index(b)
  index_a < index_b
end

def process_page_numbers(line, rules)
  pages = line.split(',').map &.to_i
  rules.each do |rule|
    return unless rule_satisfied?(pages, rule)
  end
  pages[pages.size // 2]
end

def process_rule(line)
  a, b = line.split('|', 2).map &.to_i
  {a, b}
end

rules = [] of {Int32, Int32}
rules_completed = false

answer = 0
STDIN.each_line do |line|
  if line.blank?
    rules_completed = true
  elsif rules_completed
    if mid = process_page_numbers(line, rules)
      answer += mid
    end
  else
    rules << process_rule(line)
  end
end
puts answer
