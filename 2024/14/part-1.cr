#!/usr/bin/env crystal

alias XY = {Int32, Int32}

WIDTH  = 101
HEIGHT = 103
STEPS  = 100

struct Robot
  getter position : XY
  getter velocity : XY

  def initialize(@position, @velocity)
  end

  def self.parse(line)
    match = line.match(/p=([-+]?\d+),([-+]?\d+)\s+v=([-+]?\d+),([-+]?\d+)/)
    raise "Unexpected line: #{line}" unless match
    px = match[1].to_i
    py = match[2].to_i
    vx = match[3].to_i
    vy = match[4].to_i
    new({px, py}, {vx, vy})
  end
end

class Grid
  getter width : Int32
  getter height : Int32

  @robots : Array(Robot)

  @left : Int32
  @right : Int32
  @top : Int32
  @bottom : Int32

  def initialize(@robots, @width, @height)
    @left = width // 2 - 1
    @right = @left + 2
    @top = height // 2 - 1
    @bottom = @top + 2
  end

  def step
    @robots.map! do |robot|
      rx, ry = robot.position
      vx, vy = robot.velocity
      x = (rx + vx) % width
      y = (ry + vy) % height
      Robot.new({x, y}, {vx, vy})
    end
  end

  def quadrant_counts
    q1 = 0
    q2 = 0
    q3 = 0
    q4 = 0

    @robots.each do |robot|
      x, y = robot.position
      if x <= @left && y <= @top
        q1 += 1
      elsif x >= @right && y <= @top
        q2 += 1
      elsif x <= @left && y >= @bottom
        q3 += 1
      elsif x >= @right && y >= @bottom
        q4 += 1
      end
    end

    {q1, q2, q3, q4}
  end

  def score
    quadrant_counts.product
  end

  def to_s(io : IO) : Nil
    @height.times do |y|
      @width.times do |x|
        if (x > @left && x < @right) || (y > @top && y < @bottom)
          print ' '
          next
        end
        count = @robots.count do |robot|
          robot.position[0] == x && robot.position[1] == y
        end
        char = case count
               when 0     then '.'
               when .< 10 then count.to_s[0]
               else            '+'
               end
        print char
      end
      puts
    end
  end
end

robots = STDIN.each_line.map do |line|
  Robot.parse(line)
end
grid = Grid.new(robots.to_a, WIDTH, HEIGHT)
STEPS.times { grid.step }
puts grid.score
