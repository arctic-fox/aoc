#!/usr/bin/env crystal

alias Point = {Int32, Int32}

enum Cell
  Empty
  Wall
  BoxLeft
  BoxRight
  Robot

  def box?
    box_left? || box_right?
  end

  def other_half(x, y)
    case self
    when .box_left?  then {x + 1, y}
    when .box_right? then {x - 1, y}
    else
      raise "Not a box: #{self}"
    end
  end

  def self.from_char(char : Char) : self
    case char
    when '.' then Empty
    when '#' then Wall
    when 'O' then BoxLeft
    when '[' then BoxLeft
    when ']' then BoxRight
    when '@' then Robot
    else          raise "Unrecognized cell '#{char}'"
    end
  end

  def to_char
    case self
    in .empty?     then '.'
    in .wall?      then '#'
    in .box_left?  then '['
    in .box_right? then ']'
    in .robot?     then '@'
    end
  end
end

enum Direction
  North
  South
  West
  East

  def horizontal?
    west? || east?
  end

  def vertical?
    north? || south?
  end

  def self.from_char?(char : Char) : self?
    case char
    when '^' then North
    when 'v' then South
    when '<' then West
    when '>' then East
    end
  end

  def self.from_char(char : Char) : self
    from_char?(char) || raise "Unrecognized direction '#{char}'"
  end

  def to_char
    case self
    in .north? then '^'
    in .south? then 'v'
    in .west?  then '<'
    in .east?  then '>'
    end
  end

  def offset
    case self
    in .north? then {0, -1}
    in .south? then {0, 1}
    in .west?  then {-1, 0}
    in .east?  then {1, 0}
    end
  end

  def self.each_offset(&)
    each do |direction|
      yield *direction.offset
    end
  end

  def apply(x, y)
    dx, dy = offset
    {x + dx, y + dy}
  end
end

class Grid
  getter robot : Point

  def initialize(@grid : Array(Array(Cell)))
    @robot = {0, 0}
    grid.each_with_index do |row, y|
      row.each_with_index do |cell, x|
        @robot = {x, y} if cell.robot?
      end
    end
  end

  def self.parse(io : IO) : self
    grid = [] of Array(Cell)
    io.each_line do |line|
      break if line.empty?
      row = [] of Cell
      line.chars.each do |char|
        cell = Cell.from_char(char)
        case cell
        when .box_left? then row << Cell::BoxLeft << Cell::BoxRight
        when .robot?    then row << Cell::Robot << Cell::Empty
        else                 row << cell << cell
        end
      end
      grid << row
    end
    new(grid)
  end

  def width
    @grid[0].size
  end

  def height
    @grid.size
  end

  def []?(x, y)
    return if x < 0 || y < 0 || x >= width || y >= height
    @grid[y]?.try &.[x]?
  end

  def [](x, y)
    self[x, y]? || raise IndexError.new
  end

  def []=(x, y, cell)
    @grid[y][x] = cell
  end

  def each(&)
    @grid.each_with_index do |row, y|
      row.each_with_index do |cell, x|
        yield x, y, cell
      end
    end
  end

  def each_box(&)
    each do |x, y, cell|
      yield x, y if cell.box_left?
    end
  end

  def boxes_value
    value = 0
    each_box do |x, y|
      value += y * 100 + x
    end
    value
  end

  def move(direction : Direction)
    return unless push(*@robot, direction)
    @robot = direction.apply(*@robot)
  end

  def can_push_inner?(x, y, direction : Direction)
    cell = self[x, y]
    return false if cell.wall?
    return true if cell.empty?
    rx, ry = direction.apply(x, y)
    if cell.box? && direction.vertical?
      return false unless can_push?(rx, ry, direction)
      rx, ry = direction.apply(*cell.other_half(x, y))
      return false unless can_push?(rx, ry, direction)
    else
      return false unless can_push?(rx, ry, direction)
    end
    true
  end

  def can_push?(x, y, direction : Direction)
    result = can_push_inner?(x, y, direction)
    cell = self[x, y]
    puts "Can push #{cell} at #{x},#{y} to #{direction}? #{result}" if DEBUG
    result
  end

  def push(x, y, direction : Direction)
    cell = self[x, y]
    return false if cell.wall?
    return true if cell.empty?
    return false unless can_push?(x, y, direction)
    rx, ry = direction.apply(x, y)
    return false unless push(rx, ry, direction)
    puts "Move #{cell} from #{x},#{y} to #{rx},#{ry}" if DEBUG
    self[rx, ry] = cell
    self[x, y] = Cell::Empty
    if cell.box? && direction.vertical?
      ox, oy = cell.other_half(x, y)
      rx, ry = direction.apply(ox, oy)
      cell = self[ox, oy]
      return false unless push(rx, ry, direction)
      puts "Move #{cell} from #{ox},#{oy} to #{rx},#{ry}" if DEBUG
      self[rx, ry] = cell
      self[ox, oy] = Cell::Empty
    end
    true
  end

  def to_s(io : IO) : Nil
    @grid.each do |row|
      row.each do |cell|
        io.print cell.to_char
      end
      io.puts
    end
  end
end

DEBUG = false

grid = Grid.parse(STDIN)
STDIN.each_char do |char|
  next unless direction = Direction.from_char?(char)
  STDERR.puts "Move #{direction}" if DEBUG
  grid.move(direction)
  STDERR.puts grid if DEBUG
end
puts grid.boxes_value
