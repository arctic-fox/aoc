#!/usr/bin/env crystal

answer = STDIN.each_line.count do |line|
  levels = line.split.map &.to_i
  cmp = nil
  safe = true
  levels.each_cons_pair do |a, b|
    if cmp
      break safe = false if cmp != (a <=> b)
    else
      cmp = a <=> b
      break safe = false if cmp.zero?
    end
    diff = case cmp
           when -1 then b - a
           when  1 then a - b
           else         raise "Must increase or decrease"
           end
    break safe = false unless diff.in?(1..3)
  end
  safe
end
puts answer
