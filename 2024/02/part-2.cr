#!/usr/bin/env crystal

def check_report(levels)
  index = 0
  cmp = nil
  levels.each_cons_pair do |a, b|
    if cmp
      return index if cmp != (a <=> b)
    else
      cmp = a <=> b
      return index if cmp.zero?
    end
    diff = case cmp
           when -1 then b - a
           when  1 then a - b
           else         raise "Must increase or decrease"
           end
    return index unless diff.in?(1..3)
    index += 1
  end
end

def try_modifications(levels)
  levels.each_index do |index|
    modified = levels.dup.tap &.delete_at(index)
    return true unless check_report(modified)
  end
  false
end

answer = STDIN.each_line.count do |line|
  levels = line.split.map &.to_i
  !check_report(levels) || try_modifications(levels)
end
puts answer
